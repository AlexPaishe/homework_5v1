﻿using System;

namespace HomeWork_5v5
{
    class Program
    {
        /// <summary>
        /// Расчет функции Акермана
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        static int Ack(int n, int m)
        {
            if (n == 0)
            {
                return m + 1;
            }
            else if (m == 0)
            {
                return Ack(n - 1, 1);
            }
            else
            {
                return Ack(n - 1, Ack(n, m - 1));
            }
        }

        /// <summary>
        /// Выбор значения числа n
        /// </summary>
        /// <returns></returns>
        static int N()
        {
            Console.WriteLine("Введите число n, которое должно быть в интервале от 0 до 3 включительно:");
            int n = int.Parse(Console.ReadLine());
            for(; ; )
            {
                if(n>3||n<0)
                {
                    Console.WriteLine("Число n  не может быть больше 3 или отрицательным. Повторите попытку:");
                    n = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return n;
        }

        /// <summary>
        /// Выбор значения числа m
        /// </summary>
        /// <returns></returns>
        static int M()
        {
            Console.WriteLine("Введите число m, которое должно быть в интервале от 0 до 10 включительно:");
            int m = int.Parse(Console.ReadLine());
            for (; ; )
            {
                if (m > 10 || m < 0)
                {
                    Console.WriteLine("Число m  не может быть больше 10 или отрицательным. Повторите попытку:");
                    m = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return m;
        }
        static void Main(string[] args)
        {
           Console.WriteLine( Ack(N(),M() ));
        }
    }
}
