﻿using System;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Threading.Tasks.Dataflow;

namespace HomeWork_5v3
{
    class Program
    {   /// <summary>
        /// Удаление дублированных подряд букв
        /// </summary>
        /// <param name="b"></param>
        static void Narrowing(char[] b)
        {
            int count = 0;
            for (int i = 0; i < b.Length; i++)
            {
                if (b[i] == 0)
                {
                    Console.Write(b[i]);
                }
                else
                {
                    if (b[count] == b[i])
                    {
                        b[i] = ' ';
                    }
                    else
                    {
                        if (b[count] != b[i])
                        {
                            count = i;
                            Console.Write(b[count]);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  Создание текста
        /// </summary>
        /// <returns></returns>
        static string Text()
        {
            Console.Write("Введите строку: ");
            string t = Console.ReadLine();
            return t;
        }

        static void Main(string[] args)
        {
            string text = Text();
            char[] b = text.ToCharArray();
            Narrowing(b);
        }
    }
}

 