﻿using System;

namespace HomeWork_5v1._2
{
    class Program
    {
        /// <summary>
        /// Заполнение массива.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        static void Matrix(int[,] matrix, int minValue, int maxValue)
        {
            Random rand = new Random();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(minValue, maxValue);
                    Console.Write($" {matrix[i, j],3}");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Сумма или разность массивов.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="chislo"></param>
        static void Matrix(int[,] matrix, int [,] matrix2, int a)
        {
            double[,] Mat = new double[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (a > 0)
                    {
                        Mat[i, j] = matrix[i, j] + matrix2[i, j];
                        Console.Write($" {Mat[i, j],3}");
                    }
                    else
                    {
                        if(a<0)
                        {
                            Mat[i, j] = matrix[i, j] - matrix2[i, j];
                            Console.Write($" {Mat[i, j],3}");
                        }
                    }
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Минимально допустимое число в массиве.
        /// </summary>
        /// <returns></returns>
        static int MinValue()
        {
            int minValue = 0;
            Console.WriteLine("Введите минимальное число матрицы:");
            minValue = int.Parse(Console.ReadLine());
            return minValue;

        }

        /// <summary>
        /// Максимально допустимое число в массиве.
        /// </summary>
        /// <returns></returns>
        static int MaxValue()
        {
            int maxValue = 0;
            Console.WriteLine("Введите максимальное число матрицы:");
            maxValue = int.Parse(Console.ReadLine());
            return maxValue;
        }

         /// <summary>
        ///  Число, которое производит сумму матриц или их разность.
        /// </summary>
        /// <returns></returns>
        static int A()
        {
            int a = 0;
            Console.WriteLine("Если вы желаете вычислить сумму двух матриц - нажмите положительное число.");
            Console.WriteLine("Если вы желаете вычислить разность двух матриц - нажмите отрицательное число.");
            a = int.Parse(Console.ReadLine());
            for(; ; )
            {
                if (a==0)
                {
                    Console.WriteLine("0 не положительное и не отрицательное число. Повторите попытку:");
                    a = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return a;
        }

        /// <summary>
        /// Количество строк в двухмерном массиве.
        /// </summary>
        /// <returns></returns>
        static int Line()
        {
            int Line = 0;
            Console.WriteLine("Введите число строк матрицы:");
            Line = int.Parse(Console.ReadLine());
            for (; ; )
            {
                if (Line == 0 || Line < 0)
                {
                    Console.WriteLine("Число строк не может быть равна 0 или меньше. Повторите попытку:");
                    Line = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return Line;
        }

        /// <summary>
        /// Количество столбцов в двухмерном массиве.
        /// </summary>
        /// <returns></returns>
        static int Column()
        {
            int Column = 0;
            Console.WriteLine("Введите число столбцов матрицы:");
            Column = int.Parse(Console.ReadLine());
            for (; ; )
            {
                if (Column == 0 || Column < 0)
                {
                    Console.WriteLine("Число столбцов не может быть равна 0 или меньше. Повторите попытку:");
                    Column = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return Column;
        }

        static void Main(string[] args)
        {
            int Line1 = Line();
            int Column1 = Column();
            int Min1 = MinValue();
            int Max1 = MaxValue();
            for(; ; )
            {
                if (Max1<Min1)
                {
                    Max1 = MaxValue();
                }
                else
                {
                    break;
                }
            }
            int[,] matrix1 = new int[Line1, Column1];
            Matrix(matrix1, Min1, Max1);
            int Min2 = MinValue();
            int Max2 = MaxValue();
            for(; ; )
            {
                if(Max2<Min2)
                {
                    Max2 = MaxValue();
                }
                else
                {
                    break;
                }
            }
            int[,] matrix2 = new int[Line1, Column1];
            Matrix(matrix2, Min2, Max2);
            Matrix(matrix1, matrix2, A());
        }
    }
}
