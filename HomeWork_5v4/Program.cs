﻿using System;
using System.Security.Cryptography;

namespace HomeWork_5v4
{
    class Program
    {
        /// <summary>
        /// Метод проверющий относится ли последовательность чисел к арифметической или геометрической прогрессии
        /// </summary>
        /// <param name="matrix"></param>
        static void Progression(params float [] matrix)
        {
            float d = matrix[1] - matrix[0];
            float g = matrix[1] / matrix[0];
            //Console.WriteLine("{0:0.0}", d);
            //Console.WriteLine("{0:0.0}", g);
            Console.WriteLine();
            int count = 1;
            for (int i = 0; i < matrix.Length; i++)
            {
                if (matrix[i] == matrix[0] + d * i)
                {
                    //Console.WriteLine("Арифметическая прогрессия");
                    count *= i + 1;
                }
                else
                {
                    //Console.WriteLine("Не арифмитическая прогрессия");
                    count = 0;
                }
            }
            if (count > 0)
            {
                Console.WriteLine("Арифметическая прогрессия");
            }
            else
            {
                for (int i = 0; i < matrix.Length; i++)
                {
                    count = 1;
                    if (matrix[i] == matrix[0] * Pow(g, i))
                    {
                        //Console.WriteLine("Геометрическая прогрессия");
                        count *= -1 * (i + 1);
                    }
                    else
                    {
                        //Console.WriteLine("Не геометрическая прогрессия");
                        count = 0;
                    }
                }
                if (count < 0)
                {
                    Console.WriteLine("Геометрическая прогрессия");
                }
                else
                {
                    Console.WriteLine("Не арифметическая прогрессия и не геометрическая прогрессия.");
                }
            }
        }

        /// <summary>
        /// Перенос текст в массив и разделение слов с помощью ' ',',','.','/'
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        static string[] Matrix(string t)
        {
            string[] text = t.Split(' ', '.', '/');
            return text;
        }

        /// <summary>
        ///  Создание текста
        /// </summary>
        /// <returns></returns>
        static string Text()
        {
            Console.Write("Введите последовательность чисел: ");
            string t = Console.ReadLine();
            return t;
        }

        /// <summary>
        /// Перенос чисел из одной матрицы в другую, с изменением типов данных
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        static float [] Transfer(string[] text)
        {
            float[] c = new float[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                c[i] = float.Parse(text[i]);
                //Console.Write($"{c[i]} ");
            }
            return c;
        }

        /// <summary>
        /// Возведение числа в степень
        /// </summary>
        /// <param name="g"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        static float Pow(float g,int i)
        {
            int save = 0;
            float b = 1;
            while(save!=i)
            {
                save++;
                b *= g;
            }
            return b;
        }

        static void Main(string[] args)
        {

            ////Арифметическая прогрессия = A(n+1) = A(n) + D. A(n) = a1 + d(n-1)
            ////Геометрическая прогрессия = B(n+1) = B(n)*g. B(n) = b1*g^(n-1)

            string text = Text();
            string[] matrixtext = Matrix(text);
            float[] matrix = Transfer(matrixtext);
            Progression(matrix);
        }
    }
}
 