﻿using System;

namespace HomeWork_5v2._1
{
    class Program
    {
        /// <summary>
        /// Перенос текст в массив и разделение слов с помощью ' ',',','.','/'
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        static string[] Matrix(string t)
        {
            string[] text = t.Split(' ', ',', '.', '/');
            return text;
        }

        /// <summary>
        ///  Создание текста
        /// </summary>
        /// <returns></returns>
        static string Text()
        {
            Console.Write("Введите строку: ");
            string t = Console.ReadLine();
            return t;
        }

        /// <summary>
        /// Поиск самых коротких и самых длинных слов
        /// </summary>
        /// <param name="c"></param>
        static void Matrix(string[] c)
        {
            int index = 0;
            int index2 = 0;
            int index3 = 0;
            int index4 = 0;
            int maxlen = c[0].Length;
            int maxlen2 = c[0].Length;
            int cout = 0;
            for (int i = 0; i < c.Length; i++)
            {
                if (maxlen2 == 0)
                {
                    maxlen2 = c[cout].Length;
                    cout++;
                }
                if (c[i].Length > maxlen)
                {
                    maxlen = c[i].Length;
                    index = i;
                }
                if (c[i].Length < maxlen2 && c[i].Length!=0)
                {
                    maxlen2 = c[i].Length;
                    index2 = i;
                }
                //Console.WriteLine(i);
                //Console.Write(c[i]);
                //Console.WriteLine($"\n {c[i].Length}");
            }
            //Console.WriteLine($"\n {c.Length}");
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i].Length == maxlen && i > index)
                {
                    index3 = i;
                }

                if (c[i].Length == maxlen2 && i > index2)
                {
                    index4 = i;
                }
            }
            //Console.WriteLine($"{index}  {index3} {index2}  {index4}");
            Console.WriteLine("Самое длинное слово: {0}", c[index]);
            if (c[index].Length == c[index3].Length && index!=index3)
            {
                Console.WriteLine("Самое длинное слово: {0}", c[index3]);               
            }

            Console.WriteLine("Самое короткое слово: {0}", c[index2]);
            if (c[index2].Length == c[index4].Length && index2 != index4)
            {
                Console.WriteLine("Самое короткое слово: {0}", c[index4]);
            }
        }

        static void Main(string[] args)
        {
            string text = Text();
            string[] c = Matrix(text);
            Matrix(c);
        }
    }
}
