﻿using System;

namespace HomeWork_5v1._1
{
    class Program
    {
        /// <summary>
        /// Заполнение массива.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        static void Matrix(int[,] matrix, int minValue, int maxValue)
        {
            Random rand = new Random();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(minValue, maxValue);
                    Console.Write($" {matrix[i, j],3}");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Произведение массива и числа.
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="chislo"></param>
        static void Matrix(int[,] matrix, double chislo)
        {
            double[,] Mat = new double[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Mat[i, j] = matrix[i, j] * chislo;
                    Console.Write($" {Mat[i, j],3}");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Количество строк в двухмерном массиве.
        /// </summary>
        /// <returns></returns>
        static int Line()
        {
            int Line = 0;
            Console.WriteLine("Введите число строк матрицы:");
            Line = int.Parse(Console.ReadLine());
            for (; ; )
            {
                if (Line ==0||Line<0)
                {
                    Console.WriteLine("Число строк не может быть равна 0 или меньше. Повторите попытку:");
                    Line = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return Line;
        }

        /// <summary>
        /// Количество столбцов в двухмерном массиве.
        /// </summary>
        /// <returns></returns>
        static int Column()
        {
            int Column = 0;
            Console.WriteLine("Введите число столбцов матрицы:");
            Column = int.Parse(Console.ReadLine());
            for (; ; )
            {
                if (Column == 0 || Column < 0)
                {
                    Console.WriteLine("Число столбцов не может быть равна 0 или меньше. Повторите попытку:");
                    Column = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return Column;
        }

        /// <summary>
        /// Минимально допустимое число в массиве.
        /// </summary>
        /// <returns></returns>
        static int MinValue()
        {
            int minValue= 0;
            Console.WriteLine("Введите минимальное число матрицы:");
            minValue = int.Parse(Console.ReadLine());
            return minValue;

        }

        /// <summary>
        /// Максимально допустимое число в массиве.
        /// </summary>
        /// <returns></returns>
        static int MaxValue()
        {
            int maxValue = 0;
            Console.WriteLine("Введите максимальное число матрицы:");
            maxValue = int.Parse(Console.ReadLine());
            return maxValue;
        }

        /// <summary>
        /// Число на которое будет умножаться массив.
        /// </summary>
        /// <returns></returns>
        static double N()
        {
            double n= 0;
            Console.WriteLine("Введите число на которое нужно умножить матрицу:");
            n= double.Parse(Console.ReadLine());
            for(; ; )
            {
                if(n==0)
                {
                    Console.WriteLine("На 0 умножать нельзя. Повторите попытку:");
                    n = double.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            return n;
        }

        static void Main(string[] args)
        {
            int[,] matrix = new int[Line(), Column()];
            int Min = MinValue();
            int Max = MaxValue();
            for(; ; )
                if(Min>Max)
                {
                    Max = MaxValue();
                }
                else
                {
                    break;
                }
            Matrix(matrix, Min, Max);
            Console.WriteLine("После умножения");
            Matrix(matrix, N());
            Console.ReadKey();
        }
    }
} 
